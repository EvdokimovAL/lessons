import grpc

import greeting_service_pb2
import greeting_service_pb2_grpc

def run():
    with grpc.insecure_channel('localhost:9081') as channel:
        stub = greeting_service_pb2_grpc.HelloServiceStub(channel)

        request = greeting_service_pb2.HelloRequest(firstName='asd', lastName='bbb')

        response = stub.hello(request)

    print("Greeting from server: " + response.greeting)

run()