using Grpc.Core;
using gotnet_grpc.Controllers;
using Com.Baeldung.Grpc;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
var channel = new Channel("localhost:9081", ChannelCredentials.Insecure);
//builder.Services.AddSingleton<Channel>(channel);
var helloClient = new HelloService.HelloServiceClient(channel);
//    provider.GetService<Channel>());
builder.Services.AddSingleton<HelloService.HelloServiceClient>(
    helloClient);
var provider = builder.Services.BuildServiceProvider();
builder.Services.AddSingleton<HelloServiceController>(
    new HelloServiceController(
        provider.GetService<HelloService.HelloServiceClient>()));

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
