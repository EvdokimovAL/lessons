﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Com.Baeldung.Grpc;
using Grpc.Core;

namespace gotnet_grpc.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HelloServiceController : ControllerBase
    {
        private readonly HelloService.HelloServiceClient _client;

        public HelloServiceController(HelloService.HelloServiceClient client)
        {
            _client = client ?? throw new ArgumentNullException(nameof(client));
        }
        
        [HttpGet]
        public async Task<HelloResponse> GetHello([FromQuery] string firstName, [FromQuery] string lastName)
        {
            return await _client.helloAsync(new HelloRequest { FirstName = firstName, LastName = lastName });
        }
    }
}
